---
title: "Kotitehtävä 5"
author: "majlaan@utu.fi"
date: "Päivitetty: `r Sys.time()`"
output:
  html_document:
    toc: true
    number_sections: true
    theme: united
    code_folding: hide
    toc_float:
      collapsed: false
      smooth_scroll: false
---

<!-- katso lisää http://rmarkdown.rstudio.com/html_document_format.html -->


```{r setup, include=FALSE}
library(knitr)
opts_chunk$set(list(echo=TRUE, # kaikkiin koodichunkkeihin tulee koodia. Mieti aina onko tarpeen, mutta html-dokkarissa voit käyttää YAML:ssa code_folding: hide
                    eval=TRUE, # kaikki koodikimpaleet arvioidaan
                    cache=FALSE, # koodikimpaleista ei luoda välimuistia. Raskaammissa projekteissa tätä kannattaa käyttää
                    warning=TRUE, # funktioiden varoituksia ei printata outputtiin 
                    message=TRUE, # funktioiden viestejä ei printata outputtiiin
                    fig.width=8, # kuvien oletuskorkeus
                    fig.height=8) # kuvien oletusleveys 
               )
# joitain optioita
options(scipen=999) # outputteihin tavanomaiset luvut tieteellisen merkinnäin sijaan
```

http://rpubs.com/majlaan/kotitehtava5

# Ensimmäisen tason otsikko

## Aluksi

Kiitos kurssista. Oli kyl yksi parhaita kursseja mitä oon yliopistossa suorittanut. Tein tän lopputyön yhdeltä istumalta, koska kandia pitäis vääntää eikä kerkii nyt muita kursseja käydä. Pitää harkita josko tekisin gradun sitten vaikka ärrällä kokonaan. 

Yritin viimeisessä kotitehtävässä harjoitella datojen yhdistämistä sekä faktori-asiaa, joka on vieläkin hieman mysteeri (kaikki muukin on mysteeriä ärrän kanssa). En osannut tehdä tehtävästä yhtenäistä ja järkevää kokonaisuutta, jossa olisi ollut oikeaa analyysia. 


## Datan lataamista, muokkausta, yhdistelyä ja käppyröiden piirtämistä

Päätin valita dataksi tällä sivulla <http://www.lounaistieto.fi/blog/category/yleinen/avoin-data/yhteiskunta-palvelut-avoin-data/> olevan työllisyyttä käsittelevän datan. Aloitin lataamalla datan koneelle. 

```{r}

```

Seuraavaksi luin datan ärrään. Valitsin excel-tiedoston neljännen välilehden, jossa oli tietoa kuntien työttömien määrästä tammi-syyskuussa vuonna 2015. 

```{r}
if (!file.exists("datakansio/tyottomat.RDS")){
  dir.create("./datakansio", showWarnings = FALSE) # luodaan datakansion projektin "sisälle ja siihen suhteellinen linkki" niin toimii myös muilla koneilla eri hakemistopolussa!
  download.file("http://opendata.lounaistieto.fi/aineistoja/tyomarkkinat.xlsx",
                mode="wb", destfile = "./datakansio/tyomarkkinat.xlsx")
  tyottomat <- readxl::read_excel("./datakansio/tyomarkkinat.xlsx", sheet = 4)
  saveRDS(tyottomat, "./datakansio/tyottomat.RDS")
}
tyottomat <- readRDS("./datakansio/tyottomat.RDS")
# print(tyottomat)
knitr::kable(head(tyottomat)) # mielummin
```

Muokkasin dataa poistamalla ensimmäisen rivin ja antamalla muuttujille nimet. 

```{r}
tyottomat <- tyottomat[-c(1), ]
    
names(tyottomat) <- c("nro","kunta","maakunta", "tammikuu", "helmikuu", "maaliskuu", "huhtikuu", "toukokuu", "kesakuu", "heinakuu", "elokuu", "syyskuu", "lokakuu","marraskuu", "joulukuu") 

# print(tyottomat) 
knitr::kable(head(tyottomat)) # mielummin
```

Seuraavaksi poistin datasta loka-, marras- ja joulukuut, koska näiltä kuukausilta ei ollut tietoa työttömien määrästä. 

```{r}
library(tidyverse) 
tyottomat <- tyottomat %>% 
  dplyr::select(nro, kunta, maakunta, tammikuu, helmikuu, maaliskuu, huhtikuu, toukokuu, kesakuu, heinakuu, elokuu, syyskuu)
```

Muokkasin datan vielä pitkään muotoon kuukausien osalta

```{r}
library(tidyverse) 
tyottomat <- tyottomat %>% 
  gather(., key = kuukausi, value = n, 4:12) 

# print(tyottomat)
knitr::kable(head(tyottomat)) # mielummin
```

Valitsin vielä tarkastelukunnaksi ainoastaan Turun. 

```{r}
library(tidyverse) 
tyottomat_turku <- tyottomat %>% 
  dplyr::filter(kunta %in% "Turku")
```

Tein kuukausi-muuttujasta faktorin, jotta voisin luokitella aineistoa kuvioissa kuukausien mukaan. 

```{r}
tyottomat_turku$kuukausi <- factor(tyottomat_turku$kuukausi,levels = c("tammikuu", "helmikuu", "maaliskuu", "huhtikuu", "toukokuu", "kesakuu", "heinakuu", "elokuu", "syyskuu")) 
  
```

Ja sitten piirsin viivakuvion Turun työttömien määrän kehityksestä vuonna 2015. 

```{r}
library(tidyverse) 
library(ggplot2)
pturku <- ggplot(data=tyottomat_turku, aes(x=kuukausi, y=n, group = 1)) 
pturku <- pturku + geom_line() 
pturku <- pturku + labs(x="Kuukausi", y="Työttömien määrä")
pturku <- pturku + labs(title="Työttömyyden kehitys Turussa vuonna 2015",
                        caption="Data: http://www.lounaistieto.fi/blog/2015/08/18/tyonvalitystilasto/")
pturku 

``` 

Tarkastelin myös Helsingin työttömyyttä. 

```{r}
library(tidyverse) 
tyottomat_helsinki <- tyottomat %>% 
  dplyr::filter(kunta %in% "Helsinki")

tyottomat_helsinki$kuukausi <- factor(tyottomat_helsinki$kuukausi,levels = c("tammikuu", "helmikuu", "maaliskuu", "huhtikuu", "toukokuu", "kesakuu", "heinakuu", "elokuu", "syyskuu")) 
  
```

Samanlainen viivakuvio Helsingin työttömistä:

```{r}
library(tidyverse) 
library(ggplot2)
phelsinki <- ggplot(data=tyottomat_helsinki, aes(x=kuukausi, y=n, group = 1)) 
phelsinki <- phelsinki + geom_line(color= "red") 
phelsinki <- phelsinki + labs(x="Kuukausi", y="Työttömien määrä")
phelsinki <- phelsinki + labs(title="Työttömyyden kehitys Helsingissä vuonna 2015")
phelsinki  <- phelsinki + theme(axis.text.x = element_text(angle = 90))
phelsinki 
```

Seuraavaksi yhdistin Turun ja Helsingin työttömien määrää koskevat datat

```{r}
library(tidyverse)
tyottomat_turku %>% 
  full_join(tyottomat_helsinki)-> tyottomyys_turku_ja_helsinki 
``` 

ja tarkastelin molempien kuntien työttömyyttä vierekkäin. Helsingissä näyttää olleen enemmän työttömiä vuonna 2015. 

```{r}
library(tidyverse) 
library(ggplot2)
p3 <- ggplot(data=tyottomyys_turku_ja_helsinki, aes(x=kunta, y=n, group = 1)) 
p3 <- p3 + geom_bar(stat="identity")
p3 <- p3 + labs(x="Kunta", y="Työttömien määrä")
p3 <- p3 + labs(title="Työttömien määrä Turussa ja Helsingissä vuonna 2015")
p3 
```

Otin tarkasteluun myös työttömyysasteen. Latasin työttömyysaste-datan seuraavalla komennolla (sama excel kuin aiemmin):

```{r}
tyottomyysaste <- readxl::read_excel("./datakansio/tyomarkkinat.xlsx", sheet = 3)
```

Tein uudelle datalle samoja temppuja kuin aiemmin työttömien määrää käsittelevälle datalle. 

```{r}
tyottomyysaste <- tyottomyysaste[-c(1), ]
```


```{r}
names(tyottomyysaste) <- c("nro","kunta","maakunta", "tammikuu", "helmikuu", "maaliskuu", "huhtikuu", "toukokuu", "kesakuu", "heinakuu", "elokuu", "syyskuu", "lokakuu","marraskuu", "joulukuu")
```

```{r}
library(tidyverse) 
tyottomyysaste <- tyottomyysaste %>% 
  dplyr::select(nro, kunta, maakunta, helmikuu, maaliskuu, huhtikuu, toukokuu, kesakuu, heinakuu, elokuu, syyskuu) 
```
  
```{r}
  library(tidyverse) 
tyottomyysaste <- tyottomyysaste %>% 
  gather(., key = kuukausi, value = value, 4:11)
```

Valitsin kuitenkin jatkotarkasteluun ainoastaan helmikuun 2015. Tämän tein koska dataa muokatessa tammikuusta tuli numeric-muuttujan sijaan character-muuttuja enkä osannut vaihtaa tammikuuta numeeriseksi. 

```{r} 
tyottomyysaste <- tyottomyysaste %>%
dplyr::filter(kunta %in% "Helsinki"|kunta %in% "Turku") %>%
dplyr::filter(kuukausi %in% "helmikuu") 
```

Filtteröin siis myös työllisten määrästä vain helmikuun tapaukset jatkotarkasteluun: 

```{r}
tyottomyys_tku_hel <- tyottomyys_turku_ja_helsinki %>%
dplyr::filter(kuukausi %in% "helmikuu") 
```

Yhdistin uudet datat. 

```{r}
library(tidyverse)
tyottomyysaste %>% 
  left_join(tyottomyys_tku_hel)-> viimeinen_tyottomyys 
```
  
Laskin työttömyysprosentin avulla työllisten määrän Helsingissä ja Turussa. 

```{r} 
library(tidyverse)
viimeinen_tyottomyys <- viimeinen_tyottomyys %>%
dplyr::select(kunta, value, n)%>% 
dplyr::mutate(tyolliset = n/value * 100) 
``` 

Piirsin työssäkäyvien määrästä tolppakuvioita. 

```{r}
library(tidyverse) 
library(ggplot2)
p4 <- ggplot(data=viimeinen_tyottomyys, aes(x=kunta, y=tyolliset, group = 1)) 
p4 <- p4 + geom_bar(stat="identity", fill="red", colour="orange", alpha = .2)
p4 <- p4 + theme_light()
p4 <- p4 + labs(x="Kunta", y="Töissäkäyvien määrä")
p4 <- p4 + labs(title="Töissäkäyvien määrä Turussa ja Helsingissä helmikuussa vuonna 2015")
p4
```

Lopuksi tarkastelin vielä Helsingin ja Turun työttömyysastetta.

```{r}
library(tidyverse) 
library(ggplot2)
p5 <- ggplot(data=viimeinen_tyottomyys, aes(x=kunta, y=value, group = 1)) 
p5 <- p5 + geom_bar(stat="identity", width=.2, fill="white", colour="#000000")
p5 <- p5 + theme_dark()
p5 <- p5 + labs(x="Kunta", y="Työttömyysaste %")
p5 <- p5 + labs(title="Työttömyysaste Turussa ja Helsingissä helmikuussa vuonna 2015")
p5
```

Tästä mahtavasta tarkastelusta voidaan vetää johtopäätöksenä se, että Turussa on määrällisesti vähemmän työttömiä kuin Helsingissä. Helsingin työttömyysprosentti on kuitenkin pienempi kuin Turussa. Lisäksi Helsingissä on määrällisesti enemmän työssäkäyviä ihmisiä kuin Turussa. Voisi ehkä pohtia miksi molemmissa kunnissa työttömien määrä kasvaa kesäkuukausina. Johtuuko ilmiö kenties opettajien määräaikaisista sopimuksista? 

ps. On kyl niin kätevä tää julkaiseminen (ja julkaisun muokkaus, republish-toiminto), että tästä lähtien teen kaikki pp-esitykset ärrällä suoraan rpubsiin. Tänks!

